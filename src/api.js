import axios from "axios";

const request = axios.create({ baseURL: process.env.VUE_APP_API_URL });

const api = {
  loadNamesAndTypes() {
    return request.get("/api/v1/get-names-and-types").then(({ data }) => data);
  },
  setNameAndType(data) {
    return request
      .patch("/api/v1/set-name-and-type", data)
      .then(({ data }) => data);
  },
  exportData() {
    return request.get("/api/v1/export-data").then(({ data }) => data);
  },
  fetchWarehouses() {
    return request.get("/api/v1/warehouse").then(({ data }) => data);
  },
  fetchProductsByWarehouse(id) {
    return request.get("/api/v1/warehouse/" + id).then(({ data }) => data);
  },
  saveGroups(list) {
    return request.post("/api/v1/groups", list).then(({ data }) => data);
  },
  fetchGroups() {
    return request.get("/api/v1/groups").then(({ data }) => data);
  },
  updateGroupItems(groupId, barcodes) {
    return request
      .patch("/api/v1/groups/" + groupId + "/products", barcodes)
      .then(({ data }) => data);
  },
  searchItems(warehouse, term) {
    return request
      .post("/api/v1/search", { warehouse, term })
      .then(({ data }) => data);
  },
  updateItem(item) {
    return request.patch("/api/v1/item", item).then(({ data }) => data);
  },
  fetchInsertsSummary(warehouse) {
    if (warehouse === null) warehouse = "";
    return request
      .get("/api/v1/inserts-summary/" + warehouse)
      .then(({ data }) => data);
  },
};

export default api;
