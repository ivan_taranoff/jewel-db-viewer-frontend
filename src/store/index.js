import Vuex from "vuex";
import api from "@/api";
import Vue from "vue";

Vue.use(Vuex);

export const ACTION = {
  loadWarehousesData: "loadWarehousesData",
  loadGroupsData: "loadGroupsData",
  loadProductsData: "loadProductsData",
  saveGroups: "saveGroups",
  updateProductsGroup: "updateProductsGroup",
  addGroup: "addGroup",
  performSearch: "performSearch",
  saveItem: "saveItem",
  loadInsertsSummary: "loadInsertsSummary",
  exportData: "exportData",
  loadNamesAndTypes: "loadNamesAndTypes",
  setNameAndType: "setNameAndType",
};

export const MUTATION = {
  setWarehousesData: "setWarehousesData",
  setWarehousesLoading: "setWarehousesLoading",
  setGroupsData: "setGroupsData",
  setGroupsLoading: "setGroupsLoading",
  setProductsData: "setProductsData",
  setProductsLoading: "setProductsLoading",
  setCurrentGroup: "setCurrentGroup",
  setCurrentWarehouse: "setCurrentWarehouse",
  setSearchValue: "setSearchValue",
  setInsertsData: "setInsertsData",
  setInsertsLoading: "setInsertsLoading",
  setInsertsCurrentItem: "setInsertsCurrentItem",
  setInsertsSummary: "setInsertsSummary",
  setNamesAndTypesData: "setNamesAndTypesData",
  setType: "setType",
};

export const GETTER = {
  currentGroupProducts: "currentGroupProducts",
  warehouseProductsWithoutCurrentGroup: "warehouseProductsWithoutCurrentGroup",
  productsWithGroup: "productsWithGroup",
  currentWarehouse: "currentWarehouse",
  currentGroup: "currentGroup",
  currentWarehouseGroups: "currentWarehouseGroups",
  getGroupByID: "getGroupByID",
  searchValue: "searchValue",
  insertsData: "insertsData",
  insertsCurrentItem: "insertsCurrentItem",
};

export const store = new Vuex.Store({
  state: {
    count: 0,
    warehouse: {
      data: [],
      loading: true,
    },
    groups: {
      data: [],
      loading: true,
    },
    products: {
      data: [],
      loading: true,
    },
    inserts: {
      currentBarcode: "",
      searchValue: "",
      data: [],
      loading: false,
      currentItem: null,
    },
    currentGroupID: null,
    currentWarehouseID: null,
    searchLoading: false,
    insertsSummary: { items: 0, inserts: 0 },
    namesAndTypesData: [],
    types: [],
  },
  getters: {
    [GETTER.searchValue]: (state) => state.inserts.searchValue,

    [GETTER.productsWithGroup]: (state) =>
      state.products.data.map((e) => ({ Group: -1, ...e })),

    [GETTER.currentGroupProducts]: (state, getters) =>
      getters[GETTER.productsWithGroup].filter(
        (e) => e.Group === state.currentGroupID
      ),

    [GETTER.warehouseProductsWithoutCurrentGroup]: (state, getters) =>
      getters[GETTER.productsWithGroup].filter(
        (e) => e.Group !== state.currentGroupID
      ),

    [GETTER.currentWarehouse]: (state) => {
      state.warehouse.data;
      if (state.currentWarehouseID !== null) {
        const found = state.warehouse.data.find(
          (e) => e.ID === state.currentWarehouseID
        );
        if (found) return found;
      }
      return null;
    },

    [GETTER.currentGroup]: (state) => {
      state.groups.data;
      state.currentGroupID;
      if (state.currentGroupID !== null) {
        const found = state.groups.data.find(
          (e) => e.ID === state.currentGroupID
        );
        if (found) return found;
      }
      return null;
    },

    [GETTER.currentWarehouseGroups]: (state) =>
      state.groups.data.filter((e) => e.WH === state.currentWarehouseID),

    [GETTER.getGroupByID]: (state) => (id) =>
      state.groups.data.find((e) => e.ID === id),
    [GETTER.insertsData]: (state) => state.inserts.data,
    [GETTER.insertsCurrentItem]: (state) => state.inserts.currentItem || {},
  },
  mutations: {
    [MUTATION.setNamesAndTypesData]: (state, data) => {
      state.namesAndTypesData = data;
    },

    [MUTATION.setType]: (state, data) => {
      state.types = Array.from(new Set([...data, ...state.types]));
    },
    [MUTATION.setSearchValue]: (state, data) => {
      state.inserts.searchValue = data;
      // state.groups.loading = false;
    },

    [MUTATION.setInsertsSummary]: (state, data) => {
      state.insertsSummary = data;
    },

    [MUTATION.setGroupsData]: (state, data) => {
      state.groups.data = data;
      state.groups.loading = false;
    },
    [MUTATION.setGroupsLoading]: (state, loading = true) => {
      state.groups.loading = !!loading;
    },
    [MUTATION.setProductsData]: (state, data) => {
      state.products.data = data;
      state.products.loading = false;
    },
    [MUTATION.setProductsLoading]: (state, loading = true) => {
      state.products.loading = !!loading;
    },
    [MUTATION.setWarehousesData]: (state, data) => {
      state.warehouse.data = data;
      state.warehouse.loading = false;
    },
    [MUTATION.setWarehousesLoading]: (state, loading = true) => {
      state.warehouse.loading = !!loading;
    },
    [MUTATION.setCurrentWarehouse]: (state, warehouse) => {
      state.currentWarehouseID = warehouse;
    },

    [MUTATION.setCurrentGroup]: (state, group) => {
      state.currentGroupID = group;
    },

    [MUTATION.setInsertsLoading]: (state, loading = true) => {
      state.inserts.loading = !!loading;
    },
    [MUTATION.setInsertsData]: (state, data) => {
      state.inserts.data = data;
      state.inserts.loading = false;
    },
    [MUTATION.setInsertsCurrentItem]: (state, data) => {
      state.inserts.currentItem = data;
    },
  },
  actions: {
    [ACTION.loadNamesAndTypes]: ({ commit }) => {
      return api.loadNamesAndTypes().then((data) => {
        commit(MUTATION.setNamesAndTypesData, data);
        commit(MUTATION.setType, Array.from(new Set(data.map((e) => e.Type))));
      });
    },
    [ACTION.setNameAndType]: ({ commit }, payload) => {
      return api.setNameAndType(payload).then((data) => {
        commit(MUTATION.setNamesAndTypesData, data);
        commit(MUTATION.setType, Array.from(new Set(data.map((e) => e.Type))));
      });
    },
    [ACTION.exportData]: () => api.exportData(),
    [ACTION.performSearch]: ({ commit }, { warehouse, term }) => {
      commit(MUTATION.setSearchValue, term);
      if (typeof warehouse !== "number" || !isFinite(warehouse))
        warehouse = null;
      commit(MUTATION.setInsertsLoading);
      return api
        .searchItems(warehouse, term)
        .then((data) => commit(MUTATION.setInsertsData, data))
        .finally(() => commit(MUTATION.setInsertsLoading, false));
      // }
    },

    [ACTION.saveItem]: ({ commit, dispatch }, item) => {
      commit(MUTATION.setInsertsLoading);
      return api
        .updateItem(item)
        .then(() => {
          commit(MUTATION.setSearchValue, "");
          commit(MUTATION.setInsertsData, []);
        })
        .then(() => dispatch(ACTION.loadInsertsSummary))
        .finally(() => commit(MUTATION.setInsertsLoading, false));
    },

    [ACTION.loadInsertsSummary]: ({ commit, state }) => {
      return api.fetchInsertsSummary(state.currentWarehouseID).then((data) => {
        commit(MUTATION.setInsertsSummary, data);
      });
    },

    [ACTION.loadGroupsData]: ({ commit }) => {
      commit(MUTATION.setGroupsLoading);
      return api
        .fetchGroups()
        .then((data) => {
          commit(MUTATION.setGroupsData, data);
        })
        .finally(() => {
          commit(MUTATION.setGroupsLoading, false);
        });
    },
    [ACTION.loadWarehousesData]: ({ commit }) => {
      commit(MUTATION.setWarehousesLoading);
      return api
        .fetchWarehouses()
        .then((data) => {
          commit(MUTATION.setWarehousesData, data);
        })
        .finally(() => {
          commit(MUTATION.setWarehousesLoading, false);
        });
    },
    [ACTION.loadProductsData]: ({ commit }, warehouseId) => {
      commit(MUTATION.setProductsLoading);
      return api
        .fetchProductsByWarehouse(warehouseId)
        .then((data) => {
          commit(MUTATION.setProductsData, data);
        })
        .finally(() => {
          commit(MUTATION.setProductsLoading, false);
        });
    },
    [ACTION.saveGroups]: ({ commit }, groupsList) => {
      commit(MUTATION.setGroupsLoading);
      return api
        .saveGroups(groupsList)
        .then((data) => {
          commit(MUTATION.setGroupsData, data);
        })
        .finally(() => {
          commit(MUTATION.setGroupsLoading, false);
        });
    },
    [ACTION.addGroup]: ({ dispatch, state }, Name) => {
      if (state.currentWarehouseID === null) return;
      const group = {
        ID: state.groups.data.length,
        WH: state.currentWarehouseID,
        Name,
      };
      const newData = [...state.groups.data, group];
      // commit(MUTATION.setGroupsData,newData);
      return dispatch(ACTION.saveGroups, newData).then((data) => ({
        data,
        group,
      }));
    },
    [ACTION.updateProductsGroup]: ({ commit }, { groupId, barcodes }) => {
      commit(MUTATION.setProductsLoading);
      return api
        .updateGroupItems(groupId, barcodes)
        .then((data) => {
          commit(MUTATION.setProductsData, data);
        })
        .finally(() => {
          commit(MUTATION.setProductsLoading, false);
        });
    },
  },
});
