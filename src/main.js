import Vue from "vue";
import Buefy from "buefy";
import "buefy/dist/buefy.css";
import "@fortawesome/fontawesome-free/css/fontawesome.min.css";
import "@mdi/font/css/materialdesignicons.min.css";
import VueRouter from "vue-router";
import App from "./App.vue";
import { store } from "@/store";

Vue.config.productionTip = false;

Vue.use(Buefy);
Vue.use(VueRouter);

const routes = [
  {
    path: "/old/warehouse/:id",
    name: "warehouse",
    component: () => import("./pages/WarehousePage.vue"),
  },
  {
    path: "/old/warehouse/:id/group/:group",
    name: "group",
    component: () => import("./pages/GroupPage.vue"),
  },
  {
    path: "/types",
    name: "types",
    component: () => import("./pages/TypesPage.vue"),
  },
  {
    path: "/inserts/:id?",
    name: "inserts",
    component: () => import("./pages/InsertsPage.vue"),
    alias: "/",
  },
  // { path: '/bar', component: Bar }
];

const router = new VueRouter({ routes });

new Vue({
  render: (h) => h(App),
  router,
  store,
}).$mount("#app");
